﻿using System.Text;

namespace WangbgReplace
{
    class Program
    {
        static void Main()
        {
            string targetSuffix = ".cs|.aspx|.html|.cshtml|.js|.css|.csproj|.config|.json|.txt|.sln|.pubxml|.jpg|.jpeg|.png|.user|.xml|.java|";
            string currentDirectory = Environment.CurrentDirectory;
            Console.WriteLine("项目初始化小程序");
            Console.WriteLine("1、默认区分大小写");
            Console.WriteLine("2、替换后缀默认包括[" + targetSuffix + "]");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("请输入要替换文件的后缀，用竖划线进行分隔：");
            string? targetSuffixRange = Console.ReadLine();
            if (string.IsNullOrEmpty(targetSuffixRange) == true)
            {
                targetSuffixRange = targetSuffix;
            }
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("请输入要替换的字符串：");
            string? oldKeywods = Console.ReadLine();
            if (string.IsNullOrEmpty(oldKeywods) == true)
            {
                oldKeywods = "FruitsCMS";
            }
            Console.WriteLine("请输入替换为的字符串：");
            string? newKeywods = Console.ReadLine();
            if (string.IsNullOrEmpty(newKeywods) == true)
            {
                Console.WriteLine("替换为的字符串不能为空，操作已取消，Bye...");
                return;
            }
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("替换文件的后缀包括[" + targetSuffixRange + "]");
            Console.WriteLine("确定要将路径\r\n{0}\r\n下所有符合关键词的目录、所有符合后缀的文件及其内容中\r\n{1}替换为{2}？\r\n[yes|no]", currentDirectory, oldKeywods, newKeywods);
            string? sureJudge = Console.ReadLine();
            if (string.IsNullOrEmpty(sureJudge) == false && (sureJudge.ToLower() == "yes" || sureJudge.ToLower() == "y"))
            {
                Console.WriteLine("-------------------------------------");
                Console.WriteLine("替换操作中，请稍候...");
                if (targetSuffixRange.EndsWith('|') == false)
                {
                    targetSuffixRange += "|";
                }
                JustReplace(targetSuffixRange, currentDirectory, oldKeywods, newKeywods);
                Console.WriteLine("执行成功!");
            }
            else
            {
                Console.WriteLine("您已取消替换操作，Bye...");
            }
        }

        /// <summary>
        /// 查找替换文件夹下的所有文件
        /// </summary>
        /// <param name="targetSuffixRange"></param>
        /// <param name="dealPath"></param>
        /// <param name="oldKeyWords"></param>
        /// <param name="newKeyWords"></param>
        private static void JustReplace(string targetSuffixRange, string dealPath, string oldKeyWords, string newKeyWords)
        {
            DirectoryInfo TheFolder = new(dealPath);
            if (TheFolder.Exists == false)
            {
                return;
            }
            //遍历文件
            foreach (FileInfo NextFile in TheFolder.GetFiles())
            {
                if (NextFile.Name.Contains("WangbgReplace") == true || NextFile.IsReadOnly == true)
                {
                    continue;
                }
                FileAttributes attributes = NextFile.Attributes;
                if ((attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {
                    continue;
                }
                if ((attributes & FileAttributes.System) == FileAttributes.System)
                {
                    continue;
                }
                if (targetSuffixRange.Contains(NextFile.Extension.ToLower() + "|") == false)
                {
                    continue;
                }
                //读取文件，替换内容关键词
                var fileContent = File.ReadAllText(NextFile.FullName);
                if (fileContent.Contains(oldKeyWords) == true)
                {
                    Console.WriteLine("文件内容替换：" + NextFile.Name);
                    fileContent = fileContent.Replace(oldKeyWords, newKeyWords);
                    File.WriteAllText(NextFile.FullName, fileContent);
                }
                //判断文件重命名
                if (NextFile.Name.Contains(oldKeyWords) == true)
                {
                    Console.WriteLine("文件重命名：" + NextFile.Name + "——" + NextFile.Name.Replace(oldKeyWords, newKeyWords));
                    NextFile.MoveTo(Path.Combine(dealPath + "\\" + NextFile.Name.Replace(oldKeyWords, newKeyWords)));
                }
            }
            //遍历文件夹
            foreach (DirectoryInfo NextDirectory in TheFolder.GetDirectories())
            {
                FileAttributes attributes = NextDirectory.Attributes;
                if ((attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {
                    continue;
                }
                if ((attributes & FileAttributes.System) == FileAttributes.System)
                {
                    continue;
                }
                JustReplace(targetSuffixRange, NextDirectory.FullName, oldKeyWords, newKeyWords);
            }
            //判断文件夹重命名
            if (TheFolder.Name.Contains(oldKeyWords) == true && TheFolder.Parent != null)
            {
                Console.WriteLine("文件夹重命名：" + TheFolder.Name + "——" + TheFolder.Name.Replace(oldKeyWords, newKeyWords));
                Thread.Sleep(1000);
                TheFolder.MoveTo(Path.Combine(TheFolder.Parent.FullName + "\\" + TheFolder.Name.Replace(oldKeyWords, newKeyWords)));
            }
        }
    }
}