# WangbgReplace

#### 介绍
项目初始化小工具

#### 软件架构
控制台执行程序


#### 安装教程

将编译过的exe程序，放到要替换的目录下，双击执行即可

#### 使用说明

1.  需要将exe执行文件放到要替换文件夹所在的目录，测试一把txt文件替换，把zhongguo替换为china
    ![需要将exe执行文件放到要替换文件夹所在的目录，测试一把txt文件替换，把zhongguo替换为china](https://images.gitee.com/uploads/images/2020/0217/115722_6f8f0ce0_408378.png "需要将exe执行文件放到要替换文件夹所在的目录，测试一把txt文件替换，把zhongguo替换为china")
2.  双击执行
    ![双击执行](https://images.gitee.com/uploads/images/2020/0217/115822_f91aa720_408378.png "双击执行")
3.  完事
    ![完事](https://images.gitee.com/uploads/images/2020/0217/115914_c9d1aba5_408378.png "完事")

#### 编译文件包下载

1.  X64文件运行包地址：https://www.sxwf.top/file/replace/WangbgReplace_X64.zip
2.  X86文件运行包地址：https://www.sxwf.top/file/replace/WangbgReplace_X86.zip
